import React from "react"
import {Card, CardTitle, CardBody, Form, FormGroup, Input} from "reactstrap"
import { graphql, Link, StaticQuery } from "gatsby"
import Img from "gatsby-image"

const Sidebar =() =>(
    <div>
        {/* {postAuthor && (
            <Card>
                <Img className="card-image-top" fluid={authorFluid}/>
                <CardBody>
                    <CardTitle className="text-centre text-uppercase mb-3">{postAuthor.name}</CardTitle>
                    <CardText>{postAuthor.bio}</CardText>
                </CardBody>
            </Card>
            
        )} */}
        <Card>
            <CardBody>
                <CardTitle className="text-centre  mb-3 " >
                    Latest Legal Updates
                </CardTitle>
                <Form className="text-centre">
                    <FormGroup>
                        <Input type="email"
                        name="email"
                        placeholder="your email address...">
                        </Input>
                    </FormGroup>
                    <button className="btn btn-outline-success text-uppercase">
                        Subscribe
                    </button>
                </Form>
            </CardBody>
        </Card>
        <Card>
            <CardBody>
                <CardTitle className = "text-uppercase text-centre mb-3"> 
                    Recent Posts
                </CardTitle>
                <StaticQuery 
                query={SidebarQuery} 
                render ={ data=>(
                    <div>
                        {data.allMarkdownRemark.edges.map(({node})=>(
                            <Card key= {node.id}>
                                <Link to={node.fields.slug}>
                                    <Img className = "card-image-top" fluid={node.frontmatter.image.childImageSharp.fluid}/>
                                </Link>
                                <CardBody>
                                    <CardTitle>
                                        <Link to={node.fields.slug}>
                                            {node.frontmatter.title}
                                        </Link>
                                    </CardTitle>
                                </CardBody>
                            </Card>
                        ))}
                    </div>
                    )}/>
            </CardBody>
        </Card>
    </div>
)

const SidebarQuery = graphql `
query sidebarQuerry{
    allMarkdownRemark(sort: {fields: [frontmatter___date], order: DESC}
        limit: 3) {
          edges {
            node {
                fields{
                    slug
                }
              id
              frontmatter {
                title          
                image {
                    childImageSharp {
                        fluid(maxWidth: 300) {
                            ...GatsbyImageSharpFluid
                        }
                        }
                }
              }
            }
          }
        }
}

`


export default Sidebar