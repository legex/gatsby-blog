import React from "react"
import {graphql} from "gatsby"
import Post from "../components/Post"
import Layout from "../components/layout"


const tagPosts =({data, pageContext})=>{
    const { tag } = pageContext
    const {totalCount} = data.allMarkdownRemark
    const pageHeader = `${totalCount} post${totalCount === 1 ? '' : 's'} tagged with "${tag}"`


    return(
        <Layout>
            <h1>{pageHeader}</h1>
            {data.allMarkdownRemark.edges.map(({node})=>(
                <Post 
                key={node.id}
                title={node.frontmatter.title}
                author={node.frontmatter.author }  
                date={node.frontmatter.date}
                body={node.excerpt}
                fluid={node.frontmatter.image.childImageSharp.fluid}
                tags={node.frontmatter.tags}
                slug={node.fields.slug}
              />

            ))}
        </Layout>
    )

}

export const tagQuerty = graphql`
    query($tag: String!){
        allMarkdownRemark(
            sort: { fields: [frontmatter___date], order: DESC}
            filter: { frontmatter: { tags: {in: [$tag] }}}
        ){
            totalCount
            edges{
                node{
                    id
                    frontmatter{
                        title
                        date(formatString: "MMM Do YYYY")
                        author
                        tags
                        image{
                            childImageSharp{
                                fluid(maxWidth: 650, maxHeight:371){
                                    ...GatsbyImageSharpFluid
                                }
                                    
                            }
                        }

                    }
                    fields{
                        slug
                    }
                    excerpt
                }
            }
        }
    }
`

export default tagPosts