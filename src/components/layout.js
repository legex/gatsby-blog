import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import Footer from "./Footer"

import Header from "./header"
import "../styles/index.scss"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title

          
        }
      }
    }
  `)

  return (
    <>
    <script src="https://kit.fontawesome.com/724a4075fd.js" crossOrigin="anonymous"></script>
      <Header siteTitle={data.site.siteMetadata?.title || `Title`} />
      <div className="container" id='content'>
        <main>{children}</main>
      </div>
      <Footer></Footer>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
