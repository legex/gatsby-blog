import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import {graphql, StaticQuery} from "gatsby"
import Post from "../components/Post"
import {Row, Col} from"reactstrap"
import Sidebar from "../components/sidebar"


const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Home Page</h1>
    <Row>
      <Col md="8">
      <StaticQuery query={IndexQuery} render ={ data=>{
      return(
        <div>
          {data.allMarkdownRemark.edges.map(({node})=>(
            
            <Post 
              key={node.id}
              title={node.frontmatter.title}
              author={node.frontmatter.author }  
              date={node.frontmatter.date}
              body={node.excerpt}
              fluid={node.frontmatter.image.childImageSharp.fluid}
              tags={node.frontmatter.tags}
              slug={node.fields.slug}
            />
        ))}
        </div>
      )
    }}
    />
      </Col>
      <Col md="4">
        <Sidebar/>
      </Col>
    </Row>


  </Layout>
)


const IndexQuery = graphql`
query {
  allMarkdownRemark(sort: {fields: [frontmatter___date], order: DESC }){
    edges {
      node {
        id
        fields{
          slug
        }
        frontmatter {
          title
          date(formatString: "MMM Do YYYY")
          author
          tags
          image{
            childImageSharp {
              fluid(maxWidth: 400, maxHeight: 200) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
        excerpt
      }
    }
  }
}

`



export default IndexPage
