import PropTypes from "prop-types"
import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';

const Header = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <Navbar fixed="top" light expand="sm">
      <div className="container">
      
        <NavbarBrand href="/">LEGEX INSIGHTS</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink href="/team">team</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/tags">tags</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/about">about</NavLink>
            </NavItem>
          </Nav>
        </Collapse>
    </div>
    </Navbar>
  );
}




Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
