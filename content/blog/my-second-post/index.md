---
title: My Second Post!
date: "2020-12-10T23:46:37"
author: "Vanita Arora"
image: ./test_image.jpeg
ttr: 4 Min
tags:
  - New ventures
  - ODR
---

Wow! I love blogging so much already.

Did you know that "despite its name, salted duck eggs can also be made from
chicken eggs, though the taste and texture will be somewhat different, and the
egg yolk will be less rich."?
([Wikipedia Link](https://en.wikipedia.org/wiki/Salted_duck_egg))

Yeah, I didn't either.
